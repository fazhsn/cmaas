

from collections import OrderedDict
import Classes
import Functions
import pandas as pd
import xlsxwriter
# Reading the Network Slices from the antenna File

networkSlices = open("input/antenas.csv","r")
classofService = open("input/ClassofServices.csv","r")
usersFile = open("input/userProfile.csv","r")


networkSlicesDict = {}
networkSlicesDictV2 ={}
for i,line in enumerate(networkSlices):
    if i ==0:
        continue
    Id = line.strip().split(",")[0]
    type = line.strip().split(",")[1]
    regionID = line.strip().split(",")[2]
    #subregion = line.strip().split(",")[3]
    ECI = line.strip().split(",")[4]
    availableSpectrum = line.strip().split(",")[5]
    neibNetworks = line.strip().split(",")[6]

    NetworkObj = Classes.network(Id,type,regionID,ECI, availableSpectrum)

    networkSlicesDictV2[Id] = {"Id":Id,"regionID":regionID,"ECI":int(ECI),"availableSpectrum":int(availableSpectrum)}
    networkSlicesDict[Id] = NetworkObj





classDict ={}
for i,QoS in enumerate(classofService):
    if i ==0 :
        continue

    ClassId = QoS.strip().split(",")[0]
    BWRequired = QoS.strip().split(",")[1]
    LatReq = QoS.strip().split(",")[2]

    classDict[ClassId] = {"BW":int(BWRequired),"Lat":int(LatReq)}


usersDict =  {}#OrderedDict()
userDictV2 ={}
for i,user in enumerate(usersFile):
    if i ==0:
        continue
    id = user.strip().split(",")[0]
    regionID = user.strip().split(",")[1]
    #subregion = user.strip().split(",")[2]
    Applications = user.strip().split(",")[3].split(";")
    excistingSubs,BWReqList,LatencyRqList = Functions.currentSubscriptionslice(classDict,Applications)
    userObject = Classes.Users(id,excistingSubs,BWReqList,LatencyRqList,Applications,regionID)
    BestcaseDI = round((BWReqList[0]*1000)/LatencyRqList[1],0)
    AvgCaseDI = round((BWReqList[-1]*1000)/LatencyRqList[-1],0)
    WorstCaseDI = round((BWReqList[1]*500)/LatencyRqList[0],0)
    #userDictV2 = {"id":id,"excistingSubs":excistingSubs,"BWReqList":BWReqList,"LatencyRqList":LatencyRqList,"Applications":Applications,}
    usersDict[id] = userObject
    userDictV2[id] = {"Id":id,"regionID":regionID,"ApplicationsList":Applications,"currentSub":excistingSubs,"BWReqList":BWReqList,
                      "LatencyRqList":LatencyRqList,"AvgCaseDI":AvgCaseDI,"BestCaseDI":BestcaseDI,"WorstCaseDI":WorstCaseDI}




#Finding the Maximum BestCaseDIIndex For Normalization
maxDIValue = 0
#minDIValue = 1000000000
maxTotalBW = 0
for device in usersDict.keys():
    maxDIValue= max(maxDIValue,usersDict[device].BestCaseDIndex)
    maxTotalBW = max(maxTotalBW,usersDict[device].BWReq[0])


acceptedRequests,RejectedRequests,spectrumFinalState = Functions.firstFitSpectrum(usersDict,networkSlicesDict,maxDIValue,maxTotalBW)






writer = pd.ExcelWriter("output/acceptedRequest_40x.xlsx",engine="xlsxwriter")


acceptedRequestsdf = pd.DataFrame(acceptedRequests).T
acceptedRequestsdf.to_excel(writer,sheet_name="acceptedRequest")


spectrumFinalStatedf = pd.DataFrame(spectrumFinalState).T
spectrumFinalStatedf.to_excel(writer,sheet_name="SliceFinalState")
#
# RejectedRequestsdf = pd.DataFrame(RejectedRequests).T
# RejectedRequestsdf.to_excel(writer,sheet_name="rejectedRequest")
#



writer.save()






#ForNormalization
# print(((80-0)/(21250-0))*10)
# print(((80-0)/(85-0))*10)
# print(maxTotalBW)








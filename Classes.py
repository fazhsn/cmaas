class Region:
    def __init__(self,id,users,fourGRegions,fiveGRegions):
        self.id = id
        self.user = users
        self.fourGRegions = fourGRegions
        self.fiveGRegions = fiveGRegions



class Users:
    def __init__(self,id,subscription,BWReq,LReq,TypeofApps,regionId,subregionID):
        self.id = id
        self.subscription=subscription
        self.BWReq=BWReq
        self.LReq=LReq
        #self.ChanSReq= ChanSReq
        self.BestCaseDIndex = round((self.BWReq[0]*1000)/self.LReq[1],0) # 1000 represents ms and the division represents the proportianality factor
        self.AvgCaseDIIndex = round((self.BWReq[-1]*1000)/self.LReq[-1],0)
        self.minCaseDIndex = round((self.BWReq[1]*500)/self.LReq[0],0) # 50 % of the initial Value
        #self.TotApp= TotApp
        self.TypeofApps = TypeofApps
        self.regionId = regionId
        self.subregionID = subregionID

    # def DeviceIndex(self,a,b):
    #     return a*b #+ self.ChanSReq


class network:
    def __init__(self,id,type,regionId,ECIndex,AvailableSpectrum):
        self.id = id
        self.type = type
        self.regionId = regionId
        #self.subregionID = subregionID
        self.ECIndex = round(float(ECIndex),0) # It is a SPectrum Threshold
        self.AvailableSpectrum=round(float(AvailableSpectrum),0)



# class TypesID:
#     def __init__(self,id,RequirementClass):
#


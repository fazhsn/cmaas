"""
The Below Functions define the three proposed scheduling algorithm

"""

# 1. First Fit

def firstFitSpectrum(userList,networkSlices,MaxDI,MaxBW):
    AcceptedRequest = {}
    FiveGThreshold = 5
    normalizatScaleforDeviceID = 30
    normalizatScaleforDeviceIDReduced = int(normalizatScaleforDeviceID/2)
    normalizatScaleforECI = 60
    normalizationFactorReduced = int(normalizatScaleforECI/2)
    fiveGthreshold = 0.5#percentage of the normalization Scale
    UsersTobeDegraded = {}
    UsersTobeTriedinfourG ={}
    UsersRejected = {}
    AntenasFinalState ={}
    """

    :return: Allocated Resources and the order
    """
    userECIIndexs = {}
    for ID in userList.keys():
        deviceIDReq = normalization(userList[ID].subscription,MaxDI,normalizatScaleforDeviceID)
        deviceIDReqmin = normalization(userList[ID].LReq[1],MaxDI,normalizatScaleforDeviceIDReduced)
        deviceBWReq = normalization(userList[ID].BWReq[-1],MaxBW,normalizatScaleforECI)
        deviceBWReqMin = normalization(userList[ID].BWReq[1],MaxBW,normalizationFactorReduced)
        # print("deviceIDSub: ",deviceIDReq,"deviceIDReqmin",deviceIDReqmin,"deviceBWReq",deviceBWReq,"deviceBWReqMin",deviceBWReqMin,"userList[ID].BWReq[1]",userList[ID].BWReq[1])
        # input()
        userECIIndexs[ID] = {"device_ECI":deviceIDReq,"device_min_ECI":deviceIDReqmin,"deviceBW_Req":deviceBWReq,"deviceBW_Min":deviceBWReqMin}
        for slice in networkSlices.keys():
            if networkSlices[slice].regionId ==  userList[ID].regionId and networkSlices[slice].type == "5G" :#and networkSlices[slice].subregionID ==userList[ID].subregionID :#and deviceIDReq 0.5*FiveGThreshold:
                #print("networkSlices[slice].type ",networkSlices[slice].type )
                if networkSlices[slice].ECIndex >=deviceIDReq:
                    if networkSlices[slice].AvailableSpectrum >= deviceBWReq:
                        networkSlices[slice].AvailableSpectrum = networkSlices[slice].AvailableSpectrum - deviceBWReq
                        networkSlices[slice].ECIndex = networkSlices[slice].ECIndex - deviceIDReq
                        print(ID,"Updating the Spectrum Usages")
                        AcceptedRequest[ID] = {"sliceID":slice,"AvailableSpectrum":networkSlices[slice].AvailableSpectrum,
                                               "deviceSpecRQ":deviceBWReq,"Region":userList[ID].regionId,"type":networkSlices[slice].type,
                                               "AvailableECIndex":networkSlices[slice].ECIndex,"ReqECI":deviceIDReq,"requestedECI":deviceIDReq,"RequestedSPectrum":deviceBWReq}

                        break

                    elif networkSlices[slice].AvailableSpectrum >=deviceBWReqMin:
                        print(ID,"User will be Degraded -- 5G section")
                        networkSlices[slice].AvailableSpectrum = networkSlices[slice].AvailableSpectrum - deviceBWReqMin
                        networkSlices[slice].ECIndex = networkSlices[slice].ECIndex - deviceIDReqmin
                        UsersTobeDegraded[ID] = userList[ID]

                        AcceptedRequest[ID] = {"sliceID":slice,"AvailableSpectrum":networkSlices[slice].AvailableSpectrum,
                                               "deviceSpecRQ":deviceBWReqMin,"Region":userList[ID].regionId,"type":networkSlices[slice].type,
                                               "AvailableECIndex":networkSlices[slice].ECIndex,"ReqECI":deviceIDReqmin,"requestedECI":deviceIDReqmin,"RequestedSPectrum":deviceBWReqMin}


                        break

                    else:
                        print("ID",ID," Cannot be offered service through 5G slice even with degradation Hence Will be Tried with 4G")
                        UsersTobeTriedinfourG[ID] = userList[ID]

                        #Try For Least Case
                else:
                    print("In 5G ID:",ID, " will be tried on 4G network")
                    UsersTobeTriedinfourG[ID] = userList[ID]
                    break
            else:
                #UsersTobeTriedinfourG[ID] = userList[ID]
                continue

    print("Begin of 4G loop")

    for ID in userList.keys():
        if ID not in AcceptedRequest.keys():

            deviceIDReq = normalization(userList[ID].subscription,MaxDI,normalizatScaleforDeviceIDReduced)
            deviceIDReqmin = normalization(userList[ID].LReq[1],MaxDI,normalizatScaleforDeviceIDReduced)
            deviceBWReq = normalization(userList[ID].BWReq[-1],MaxBW,normalizatScaleforECI)
            deviceBWReqMin = normalization(userList[ID].BWReq[1],MaxBW,normalizationFactorReduced)

            for slice in networkSlices.keys():
                if networkSlices[slice].regionId == userList[ID].regionId and networkSlices[slice].type == "4G" :
                    if networkSlices[slice].ECIndex >= deviceIDReq:
                        if networkSlices[slice].AvailableSpectrum >= deviceBWReq:
                            networkSlices[slice].AvailableSpectrum = networkSlices[slice].AvailableSpectrum - deviceBWReq
                            networkSlices[slice].ECIndex = networkSlices[slice].ECIndex - deviceIDReq
                            print("Updating the Spectrum Usages", )
                            AcceptedRequest[ID] = {"sliceID":slice,"AvailableSpectrum":networkSlices[slice].AvailableSpectrum,
                                                   "deviceSpecRQ":userECIIndexs[ID]["deviceBW_Req"],"Region":userList[ID].regionId,"type":networkSlices[slice].type,
                                                   "AvailableECIndex":networkSlices[slice].ECIndex,"ReqECI":userECIIndexs[ID]["device_ECI"],"requestedECI":deviceIDReq,"RequestedSPectrum":deviceIDReq}

                            break
                        elif networkSlices[slice].AvailableSpectrum >= deviceBWReqMin:
                            """
                            Write the Module as above where I fit the minimum requirement and fir it in the same 5G slice
                            """
                            print(ID, "User will be Degraded")
                            networkSlices[slice].AvailableSpectrum = networkSlices[slice].AvailableSpectrum - deviceBWReqMin
                            networkSlices[slice].ECIndex = networkSlices[slice].ECIndex - deviceIDReqmin
                            UsersTobeDegraded[ID] = userList[ID]
                            AcceptedRequest[ID] = {"sliceID":slice,"AvailableSpectrum":networkSlices[slice].AvailableSpectrum,
                                                   "deviceSpecRQ":userECIIndexs[ID]["deviceBW_Req"],"Region":userList[ID].regionId,"type":networkSlices[slice].type,
                                                   "AvailableECIndex":networkSlices[slice].ECIndex,"ReqECI":userECIIndexs[ID]["device_ECI"],"requestedECI":deviceIDReqmin,"RequestedSPectrum":deviceBWReqMin}

                            break
                    else:
                        print("ID", ID,
                              " Cannot be offered service through 5G slice even with degradation Hence Will be Tried with 4G")
                        UsersRejected[ID] = ID
        else:
            continue




         #print("TO be tried on 4G",i)
    for i in AcceptedRequest.keys():
        print(i,AcceptedRequest[i])

    for k in UsersRejected.keys():
        print(UsersRejected[k])


    for slice in networkSlices.keys():
        AntenasFinalState[slice] = {"Type":networkSlices[slice].type,"ID":slice,"AvailableECI":networkSlices[slice].ECIndex,
                                    "AvailableSpectrum":networkSlices[slice].AvailableSpectrum,"RegionID":networkSlices[slice].regionId}


    return AcceptedRequest,UsersRejected,AntenasFinalState
    #             elif
    #
    #             else:
    #                 print(ID," User Will be Rejected")
    #                 UsersRejected[ID] = userList[ID]
    #                 break
    #
    #
    # pass


#2. Assscending Order

def asscendingOrderSpectrum(userList,networkSlices):
    """

    :return: Allocated Resouces and the Order
    """

    pass

#3.
def relativeSpectrumLoss(userList,networkSlices):
    """


    :return: Allocated resource and the order
    """

    pass



def RelativeSpectrumLossEstimate():
    pass


def DCICalculationfromClassoFService():
    pass



def currentSubscriptionslice(QoSClasses,listofApplications):
    TotBWRequired = 0
    maxBw = 0
    minBW = 100
    minLatency = 50 #ms
    maxlatency = 50#ms
    TotalLatency = 0
    #minLatency = 0 #ms
    TotalAppliccations = len(listofApplications)
    for app in listofApplications:
        TotBWRequired = TotBWRequired + QoSClasses[app]["BW"]
        maxBw = max(maxBw,QoSClasses[app]["BW"])
        minBW = min(minBW,QoSClasses[app]["BW"])
        minLatency = min(QoSClasses[app]["Lat"],minLatency)
        maxlatency = max(QoSClasses[app]["Lat"],maxlatency)
        TotalLatency = TotalLatency + QoSClasses[app]["Lat"]

    AvgLatency = round(TotalLatency/TotalAppliccations,1)
    AvgBW = round(TotBWRequired/TotalAppliccations,1)

    return round((maxBw*1000)/minLatency,0),[TotBWRequired,minBW,AvgBW],[maxlatency,minLatency,AvgLatency]



def normalization(currentValue,maxValue,normalizedTo):
    return round(((currentValue-0)/(maxValue-0))*normalizedTo,3)









